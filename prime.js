module.exports = function(context, cb) {
    cb(null, { result: isPrime(parseInt(context.query.number)) });
}

function isPrime(num) {
    for ( var i = 2; i < num; i++ ) {
        if ( num % i === 0 ) {
            return false;
        }
    }
    return true;
}