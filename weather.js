var request = require('request');

module.exports =
  function (context, cb) {
    var api_key = "bda2087028ffba86f97a1496db115204";
    var city = context.query.city;
    var url     = "http://api.openweathermap.org/data/2.5/forecast/city?q=" + city + "&APPID=bda2087028ffba86f97a1496db115204";

    request.get(url, function (error, res, body) {
        if (error)
          cb(error);
        else
          data = JSON.parse(body);
          result = {
            coord: data["city"]["coord"],
            weather: data["list"][0]["main"], summary: data["list"][1]["weather"][0]
          }
          cb(null, result);
    });
  }